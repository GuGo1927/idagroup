import * as types from "./types";
import * as mutation_types from "./mutation_types";

export default {
  [types.A_SAVE_PAYMENT_DATA](context, payload) {
    context.commit(mutation_types.M_SAVE_PAYMENT_DATA, payload);
  },
  [types.A_ADD_NEW_PAYMENT](context, payload) {
    context.commit(mutation_types.M_ADD_NEW_PAYMENT, payload);
  }
};
