import * as mutation_types from "./mutation_types";

export default {
  [mutation_types.M_SAVE_PAYMENT_DATA](state, payload) {
    state.paymentData = payload;
  },
  [mutation_types.M_ADD_NEW_PAYMENT](state, payload) {
    state.paymentHistory.push(payload);
  }
};
