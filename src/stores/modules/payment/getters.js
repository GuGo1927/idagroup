export default {
    getPaymentData: state => {
        return state.paymentData || [];
    },
    getHistory: state => {
        return state.paymentHistory || [];
    }
}