import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import { store } from "./stores/store";

Vue.config.productionTip = false;

import VueMask from 'v-mask'
Vue.use(VueMask);
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

Vue.use(require('vue-moment'));

import { ClientTable } from 'vue-tables-2';
Vue.use(ClientTable);
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
