import Vue from "vue";
import Router from "vue-router";
import Payment from "./components/payment/Payment";
import Info from "./components/paymentInfo/Info";
import History from "./components/paymentHistory/History";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "payment",
      component: Payment
    },
    {
      path: "/payment-info",
      name: "payment-info",
      component: Info
    },
    {
      path: "/payment-history",
      name: "payment-history",
      component: History
    },
  ]
});
